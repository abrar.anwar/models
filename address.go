package models

type Address struct {
	Id     string `json:"id"`
	Street string `json:"street"`
}
